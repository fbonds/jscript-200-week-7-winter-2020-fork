// TODO
const selectEl = document.getElementById('contact-kind');

const setSelectValidity = function() {
    if (selectEl.value === 'choose') {
        selectEl.setCustomValidity("Must select an option");
    } else {
        selectEl.setCustomValidity('');
    }
}


setSelectValidity();
selectEl.addEventListener('change', setSelectValidity());
