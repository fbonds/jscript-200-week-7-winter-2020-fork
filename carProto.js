/**
 * Car class
 * @constructor
 * @param {String} model
 */

//  class car {
//      constructor(currentSpeed, model) {
//          this.currentSpeed = 0;
//          model() {
//              this.model = model;
//          }
//          accelerate() {
//              this.currentSpeed += 1;
//          }
//          brake() {
//              this.currentSpeed -= 1;
//          }
//          toString() {

//          }

//      }
//  }

 class Car {
     constructor(model) {
         this.currentSpeed = 0;
         this.model = model;
     }
     accelerate() {
         this.currentSpeed++;
     }
     brake() {
         this.currentSpeed--;
     }
     toString() {
         return `${this.model} is currently going ${this.currentSpeed} MPH`;

     }
 };

 const corolla = new Car('Corolla');
 corolla.accelerate();
 corolla.accelerate();
 corolla.brake();
 console.log(corolla.toString());

//  Create an instance, accelerate twice, brake once, and console log the instance.toString()

/**
 * ElectricCar class
 * @constructor
 * @param {String} model
 */

class ElectricCar extends Car {
    constructor(model) {
        super(model);
        this.motor = 'electric';
    }
    accelerate() {
        super.accelerate();
        super.accelerate();
    }
    toString() {
        return `${this.model} is currently going ${this.currentSpeed} MPH`;
    }

    }

//  Create an instance, accelerate twice, brake once, and console log the instance.toString()
const tesla = new ElectricCar('Tesla');
 tesla.accelerate();
 tesla.accelerate();
 tesla.brake();
 console.log(tesla.toString());
