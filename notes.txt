constraint validation on developer.mozilla.org good reference for html attributes.
delete 'novalidate'

objects are key:value pairs string, number, function

class Cat {
  constructor(name, age) {
this.name = name; this.age = age; this.talk = function() {
console.log(`${this.name} says Meow`); }
} };
const waffles = new Cat('Waffles', 7);

prototypes

class Cat {
  constructor(name, age) {
this.name = name;
this.age = age; }
talk() {
console.log(`${this.name} says Meow`); }
};
const waffles = new Cat('Waffles', 7);

extending:

class Car {
  constructor(make, model, maxSpeed) {
this.make = make; this.model = model; this.maxSpeed = maxSpeed;
} honk() {
console.log('Beep'); }
toString() {
return `${this.make} ${this.model} with a max speed of ${this.maxSpeed}`;
} }
class Porsche extends Car {
  constructor(model, maxSpeed) {
    super('Porsche', model, maxSpeed);
this.turboCharged = true; }
}
const nineEleven = new Porsche('911', 160); console.log(nineEleven.toString()); nineEleven.honk();